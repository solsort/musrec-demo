function u16_fp16(u) {
  let e = (u & 0x7c00) >> 10;
  let f = u & 0x03ff;
  return (
    (u & 0x8000 ? -1 : 1) *
    (e
      ? Math.pow(2, e - 15) * (1 + f / Math.pow(2, 10))
      : Math.pow(2, -14) * (f / Math.pow(2, 10)))
  );
}
let pca;
async function initPCA() {
  let dl = await fetch("smk_creator_pca_44484x128fp16.bin");
  dl = await dl.arrayBuffer();
  dl = new Uint16Array(dl);
  pca = Float32Array.from(dl);
  for (let i = 0; i < dl.length; ++i) {
    pca[i] = u16_fp16(dl[i]);
  }
  normaliseData();
}
let data;
async function initData() {
  let dl = await fetch("smk_creator_data.json");
  dl = await dl.json();
  data = dl.map(([thumb, obj, creator, title, start, end, placement], pos) => ({
    pos,
    creator,
    title,
    thumb,
    obj,
    start,
    end,
    placement
  }));
}
function normaliseVector(a) {
  let size = 0;
  for (let i = 0; i < 128; ++i) {
    let x = pca[a * 128 + i];
    size += x * x;
  }
  let scale = 1 / Math.sqrt(size);
  if (window.preferExhibition && data[a].placement) {
    scale = scale * 1.4;
  }
  for (let i = 0; i < 128; ++i) {
    pca[a * 128 + i] *= scale;
  }
}
sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
function normaliseData() {
  for (let i = 0; i < pca.length / 128; ) {
    normaliseVector(i++);
  }
}
window.normaliseData = normaliseData;
function dot(a, b) {
  a = 128 * a;
  b = 128 * b;
  let max = a + 128;
  let sum = 0;
  while (a < max) {
    sum += pca[a] * pca[b];
    ++a;
    ++b;
  }
  return sum;
}
function recommend(a, n) {
  let dots = [];
  let pos = [];
  for (let i = 0; i < n; ++i) {
    dots[i] = 0;
  }
  let minDot = 0;

  for (let b = 0; b < data.length; ++b) {
    let d = dot(a, b);
    if (d > minDot) {
      for (let i = 0; i < n; ++i) {
        if (dots[i] === minDot) {
          pos[i] = b;
          dots[i] = d;
          minDot = Math.min.apply(null, dots);
          break;
        }
      }
    }
  }
  let result = [];
  for (let i = 0; i < n; ++i) {
    result.push({ weight: dots[i], ...data[pos[i]] });
  }
  result.sort((a, b) => b.weight - a.weight);
  return result;
}

function recommendSample() {
  img = ptr.img;
  if (!img) {
    return data[(Math.random() * data.length) | 0];
  }
  let samples = [];
  for (let i = 0; i < 7000; ++i) {
    samples.push((Math.random() * data.length) | 0);
  }
  let curDot = -1;
  let current = 0;
  let imseen = imgs.map(o => o.data.pos);
  for (let i = 0; i < data.length; ++i) {
    /*
    let seen = false;
    for(j=0;j<imgs.length;++j) {
      if(imseen[i] === samples[i]) {
        seen = true;
      }
    }
    if(seen) {
      continue;
    }
    */

    if (dot(samples[i], img.pos) > curDot && !imseen.includes(samples[i])) {
      current = samples[i];
      curDot = dot(samples[i], img.pos);
    }
  }
  return data[current];
}

window.preferExhibition = true;
window.LimitCreators = true;
let ptr = {};
async function main() {
  let p = initData();
  initPCA();
  await p;
  //await Promise.all([initPCA(), initData()]);
  let main = document.querySelector("#main");
  main.onpointerdown = e => {
    Object.assign(ptr, { down: true, x: e.clientX, y: e.clientY });
    setCurrentImage(e);
    e.preventDefault();
  };
  main.onpointermove = e => {
    Object.assign(ptr, { x: e.clientX, y: e.clientY });
    setCurrentImage(e);
    e.preventDefault();
  };
  main.onpointerup = () => (ptr.down = false);
  main.innerHTML = "";
  setInterval(updateImgs, 20);
}

const imgCount = 50;
let imgs = [];
let imgElems = [];
const speed = 1.01;
let size = Math.sqrt((window.innerWidth * window.innerHeight) / imgCount) * 1.5;
const barHeight = 100;
function setCurrentImage(e) {
  const img = e.srcElement;
  ptr.img = null;
  for (let i = 0; i < imgElems.length; ++i) {
    if (imgElems[i] === img) {
      ptr.img = imgs[i].data;
    }
  }
  console.log(ptr.img);
  if (ptr.img) {
    let o = ptr.img;
    let [l, f] = o.creator.split(",");
    bar.innerHTML = `<a href="https://open.smk.dk/artwork/image/${o.obj}">
    <img src="${o.thumb.replace("!1600", "!400")}"> 
    <div>&nbsp;</div>
    <div class="creator">${f + " " + l}</div>
    <div class="title">${o.title} <b>(${o.end})</b></div>
    </a>
    `;

    bar.ontouchstart = () => location.href=`https://open.smk.dk/artwork/image/${o.obj}`;
  } else {
    bar.innerHTML = "";
    bar.ontouchstart = () => {}
  }
}
function updateImgs() {
  let main = document.querySelector("#main");
  main.style.height = window.innerHeight - barHeight + "px";
  if (imgs.length < imgCount) {
    imgs.unshift({
      x: Math.random() * window.innerWidth,
      y: Math.random() * (window.innerHeight - barHeight),
      data: recommendSample()
    });
    let elem = document.createElement("img");
    elem.src = imgs[0].data.thumb.replace("!1600", "!400");
    elem.style.opacity = 0;
    elem.style.maxWidth = size + "px";
    elem.style.maxHeight = size + "px";
    elem.onload = e => (e.target.style.opacity = 1);
    imgElems.unshift(elem);
    main.prepend(elem);
  }
  if (ptr.down) {
    for (let i = 0; i < imgs.length; ++i) {
      let o = imgs[i];
      o.x = (o.x - ptr.x) * speed + ptr.x;
      o.y = (o.y - ptr.y) * speed + ptr.y;
      if (
        o.x < -size / 2 ||
        o.y < -size / 2 ||
        o.x > window.innerWidth + size / 2 ||
        o.y > window.innerHeight - barHeight + size / 2
      ) {
        o.delete = true;
      }
    }
  }
  for (let i = 0; i < imgs.length; ++i) {
    let o = imgs[i];
    let e = imgElems[i];
    Object.assign(e.style, { top: `${o.y}px`, left: `${o.x}px` });
  }
  let j = 0;
  for (i = 0; i < imgs.length; ++i) {
    if (imgs[i].delete) {
      imgElems[i].parentNode.removeChild(imgElems[i]);
    }
    if (!imgs[i].delete) {
      imgs[j] = imgs[i];
      imgElems[j] = imgElems[i];
      ++j;
    }
  }
  imgs = imgs.slice(0, j);
  imgElems = imgElems.slice(0, j);
}

main();

document.addEventListener(
  "touchmove",
  e => {
    e.preventDefault();
  },
  false
);
document.addEventListener(
  "touchend",
  e => {
    e.preventDefault();
  },
  false
);
window.oncontextmenu = () => {
       event.preventDefault();
       event.stopPropagation();
       return false;
};
