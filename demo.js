function u16_fp16(u) {
  let e = (u & 0x7c00) >> 10;
  let f = u & 0x03ff;
  return (
    (u & 0x8000 ? -1 : 1) *
    (e
      ? Math.pow(2, e - 15) * (1 + f / Math.pow(2, 10))
      : Math.pow(2, -14) * (f / Math.pow(2, 10)))
  );
}

let pca;
async function initPCA() {
  let dl = await fetch("smk_creator_pca_44484x128fp16.bin");
  dl = await dl.arrayBuffer();
  dl = new Uint16Array(dl);
  pca = Float32Array.from(dl);
  for (let i = 0; i < dl.length; ++i) {
    pca[i] = u16_fp16(dl[i]);
  }
  normaliseData();
}
let data;
async function initData() {
  let dl = await fetch("smk_creator_data.json");
  dl = await dl.json();
  data = dl.map(([thumb, obj, creator, title, start, end, placement], pos) => ({
    pos,
    creator,
    title,
    thumb,
    obj,
    start,
    end,
    placement
  }));
}
function normaliseVector(a) {
  let size = 0;
  for (let i = 0; i < 128; ++i) {
    let x = pca[a * 128 + i];
    size += x * x;
  }
  let scale = 1 / Math.sqrt(size);
  if (window.preferExhibition && data[a].placement) {
    scale = scale * 1.4;
  }
  for (let i = 0; i < 128; ++i) {
    pca[a * 128 + i] *= scale;
  }
}
sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
function normaliseData() {
  for (let i = 0; i < pca.length / 128; ) {
    normaliseVector(i++);
  }
}
window.normaliseData = normaliseData;
function dot(a, b) {
  a = 128 * a;
  b = 128 * b;
  let max = a + 128;
  let sum = 0;
  while (a < max) {
    sum += pca[a] * pca[b];
    ++a;
    ++b;
  }
  return sum;
}
function recommend(a, n) {
  let dots = [];
  let pos = [];
  for (let i = 0; i < n; ++i) {
    dots[i] = 0;
  }
  let minDot = 0;

  for (let b = 0; b < data.length; ++b) {
    let d = dot(a, b);
    if (d > minDot) {
      for (let i = 0; i < n; ++i) {
        if (dots[i] === minDot) {
          pos[i] = b;
          dots[i] = d;
          minDot = Math.min.apply(null, dots);
          break;
        }
      }
    }
  }
  let result = [];
  for (let i = 0; i < n; ++i) {
    result.push({ weight: dots[i], ...data[pos[i]] });
  }
  result.sort((a, b) => b.weight - a.weight);
  return result;
}
function showHTML(html) {
  window.scrollTo(0, 0);
  document.getElementById("main").innerHTML = `
    ${html}
    <br>
    <center>
      <!-- div class="button" onclick="randomImage()" >Tilfældigt billede</div-->
      <div style="margin: 8px;">
      <input id="search" value="${
        "" /*searchQuery()*/
      }" onchange="search()" /><b style="font-size: 150%" onclick="search()">🔍</b>
      </div>
      <div class="button" onclick="location.hash=''"> Tilfældige billeder </div>
      <div class="button" onclick="window.preferExhibition = !window.preferExhibition; normaliseData();showImage()"> Prioritér ${
        window.preferExhibition ? "ikke" : ""
      } udstillede værker </div>
      <div class="button" onclick="window.limitCreators = !window.limitCreators; showImage()"> Begræns ${
        window.limitCreators ? "ikke" : ""
      } anbefalinger til maks 7 værker af samme kunstner.  </div>
      <br>
      <a href="explore.html"><div class="button">Gå på opdagelse...</div> </a>
    </center>

    `;

  //document.getElementById("randomButton").onclick = randomImage;
}
function searchQuery() {
  try {
    const searchField = document.getElementById("search");
    return searchField.value.trim().toLowerCase();
  } catch (e) {
    return "";
  }
}
window.search = () => {
  const query = searchQuery();
  let result = data.filter(
    o =>
      o.obj.toLowerCase() === query ||
      o.creator.toLowerCase().indexOf(query) !== -1 ||
      o.title.toLowerCase().indexOf(query) !== -1
  );
  if (!result.length) {
    result = [
      {
        creator: "Ingen resultater matcher søgningen",
        start: "",
        end: "",
        title: "Ingen resultater matcher søgningen",
        thumb: data[4695].thumb,
        obj: "",
        placement: "",
        pos: ""
      }
    ];
  }
  if (window.preferExhibition) {
    const exhibited = result.filter(o => o.placement);
    if (exhibited.length >= 80) {
      result = exhibited;
    } else if (exhibited.length) {
      result = exhibited.concat(result.slice(0, 80 - exhibited.length));
    }
  }

  let random = [];
  for (let i = 0; i < 80 && result.length; ++i) {
    let pos = (Math.random() * result.length) | 0;
    random.push(result[pos]);
    result[pos] = result[result.length - 1];
    result.pop();
  }
  showHTML(imagesHTML(random));
};
function showImage() {
  if (location.hash.slice(1) === "") {
    return search();
  }
  let id = +location.hash.slice(1);
  let o = data[id];
  let recommended = recommend(id, 2000);
  recommended = recommended.filter(o => o.pos !== id);
  if (window.limitCreators) {
    const newRec = [];
    const creatorCount = {};
    for (let o of recommended) {
      creatorCount[o.creator] = (creatorCount[o.creator] || 0) + 1;
      if (creatorCount[o.creator] <= 7) {
        newRec.push(o);
      }
    }
    recommended = newRec;
  }
  showHTML(
    `
  <div id="primary">
    <img style="height: 100px" class="mainImg" src="${o.thumb.replace(
      "!1600",
      "!1600"
    )}" onload="newMainImg(this)"> 
    <div id=mainDesc>
      ${o.title} &nbsp; <b>(${
      o.start === o.end ? o.end : o.start + "-" + o.end
    })</b><br>
      <b>${o.creator}</b><br>
      ${o.placement ? "Udstillet på SMK, " + o.placement + "<br>" : ""}
      <a href="${"https://open.smk.dk/artwork/image/" +
        o.obj}">Åbn dette billede på open.smk.dk, <b>SMK</b>&nbsp;OPEN</big></a>
      </div>
  </div>
  ` + imagesHTML(recommended.slice(0, 100))
  );
}
window.newMainImg = img => {
  const parent = img.parent;
  const height = Math.min(
    window.innerHeight - 100,
    (img.height * (window.innerWidth - 72)) / img.width
  );
  document.getElementById("mainDesc").style.maxWidth =
    Math.floor((height * img.width) / img.height - 20) + "px";
  img.style.height = height + "px";
};
function imagesHTML(l) {
  return l
    .map(
      ({
        creator,
        thumb,
        pos,
        end,
        title
      }) => `<a class="thumb" href="#${pos}"><div class=thumbDiv>
  <img height="120" src="${thumb.replace(
    "!1600",
    "!240"
  )}" onload="handleThumbLoad(this);" />
  <div class=thumbMeta>
  ${creator}<br /> 
  ${end} / ${title}
  </div>
  </div></a>`
    )
    .join(" ");
}
async function main() {
  await Promise.all([initPCA(), initData()]);
  showImage();
}
window.randomImage = function randomImage() {
  location.hash = "#" + ((Math.random() * data.length) | 0);
  window.onhashchange();
  showImage();
};
window.handleThumbLoad = elem => {
  elem.parentElement.style.width = elem.width + "px";
};
let prevWidth;
window.onresize = () => {
  if (window.innerWidth !== prevWidth) {
    showImage();
    prevWidth = window.innerWidth;
  }
};
window.onhashchange = () => {
  showImage();
};
window.preferExhibition = true;
window.LimitCreators = true;
main();
